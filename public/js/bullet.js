function Bullet(x, y) {
    Element.call(this, x, y)
}
Bullet.prototype.move = function() {
    this.y = this.y - CONFIG.bulletSpeed
}
Bullet.prototype.draw = function() {
    context.beginPath()
    context.moveTo(this.x, this.y)
    context.lineTo(this.x, this.y - CONFIG.bulletSize)
    context.lineWidth = 1;//设置线条宽度
    context.strokeStyle = "#fff";//设置线条颜色
    context.stroke();//用于绘制线条
}