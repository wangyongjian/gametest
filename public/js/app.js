// $('#game').css({'width': $(window).get(0).innerWidth, 'height': $(window).get(0).innerHeight})
// $('#canvas').css({'width': $(window).get(0).innerWidth, 'height': $(window).get(0).innerHeight})
// 元素
var container = document.getElementById('game');
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var padding = 30;
var allLevel = document.querySelector('.allLevel');
allLevel.innerHTML = CONFIG.totalLevel;
var score1 = document.querySelector('.score1');//failed
var score2 = document.querySelector('.score2');//success
var score3 = document.querySelector('.score3');// all-success
var score = 0;
var enemyPlace = {
  minY: padding, //30
  maxY: canvas.height - padding - CONFIG.planeSize.height - CONFIG.enemySize, //420
  minX: padding,//30
  maxX: canvas.width - padding - CONFIG.enemySize //620
}
var planePlace = {
  Y: canvas.height - padding - CONFIG.planeSize.height, //470
  minX: padding, //30
  maxX: canvas.width - padding - CONFIG.planeSize.width, //610
}
var sources = [
  CONFIG.enemyIcon,
  CONFIG.enemyBoomIcon,
  CONFIG.planeIcon
]
var self = {};
var enemyXArr=[];
function num(a,b){
  return a-b;
}

/**
 * 整个游戏对象
 */
var GAME = {
  /**
   * 初始化函数,这个函数只执行一次
   * @param  {object} opts 
   * @return {[type]}      [description]
   */
  init: function (opts) {
    this.status = 'start';
    
    // console.log(sources)
    self = this;
    loadImages(sources, function (loadedImages) {
      CONFIG.enemyIcon = loadedImages[0];
      CONFIG.enemyBoomIcon = loadedImages[1];
      CONFIG.planeIcon = loadedImages[2];
      self.bindEvent();
    });
    // plane.draw(loadedImages[2], CONFIG.planeSize.width, CONFIG.planeSize.height)

  },
  bindEvent: function () {
    var self = this;
    var playBtn = document.querySelector('.js-play');
    var nextBtn = document.querySelector('.js-next');
    var againBtn = document.querySelector('.js-again');
    var replayBtn = document.querySelector('.js-replay'); 
    // 开始游戏按钮绑定
    playBtn.onclick = function () {
      self.play();
    };
    //继续游戏按钮绑定
    nextBtn.onclick = function () {
      CONFIG.lineNums++
      self.play();
    };
    //再来一次按钮绑定
    againBtn.onclick = function () {
      CONFIG.lineNums = 1
      score = 0;
      self.play();
    };
    //重新开始按钮绑定
    replayBtn.onclick = function () {
      score = 0;
      CONFIG.lineNums = 1
      self.play();
    };
  },
  /**
   * 更新游戏状态，分别有以下几种状态：
   * start  游戏前
   * playing 游戏中
   * failed 游戏失败
   * success 游戏成功
   * all-success 游戏通过
   * stop 游戏暂停（可选）
   */
  setStatus: function (status) {
    this.status = status;
    container.setAttribute("data-status", status);
  },
  play: function () {
    canvas.style.display = 'block'
    var self = this
    this.setStatus('playing');
    var plane = new Plane((planePlace.minX + planePlace.maxX) / 2, planePlace.Y)
    var bullets = []
    var lineNums = CONFIG.lineNums
    // var enemyNums = CONFIG.numPerLine * lineNums
    var enemys = []
    var planeDirection = 'stop'
    var bulletShoot = false
    for (let i = 0; i < lineNums; i++) {
      for (let j = 0; j < CONFIG.numPerLine; j++) {
        // enemys.push(new Enemy(padding, padding))
        // console.log(new Enemy(padding + (j % CONFIG.numPerLine) * (CONFIG.enemyGap + CONFIG.enemySize), padding + (CONFIG.enemySize + padding) * i), i, j)
        enemys.push(new Enemy(padding + (j % CONFIG.numPerLine) * (CONFIG.enemyGap + CONFIG.enemySize), padding + (CONFIG.enemySize) * i))
      }
    }
    function keyEvent(){
      document.onkeydown = function (e) {
        // 获取被按下的键值 (兼容写法)
        var key = e.keyCode || e.which || e.charCode;
        switch (key) {
          // 点击左方向键
          case 37:
          planeDirection = 'left'
            break;
          // 点击右方向键
          case 39:
          planeDirection = 'right'
            break;
          // 点击空格
          case 32:
          bulletShoot = true
          plane.shoot(bullets)
            break;
        }
      };
      document.onkeyup = function (e) {
        // 获取被按下的键值 (兼容写法)
        var key = e.keyCode || e.which || e.charCode;
        switch (key) {
          // 点击左方向键
          case 37:
          planeDirection = 'stop'
            break;
          // 点击右方向键
          case 39:
          planeDirection = 'stop'
            
            break;
          // 点击空格
          case 32:
          // plane.shoot(bullets)
          bulletShoot = false
            break;
        }
      };
    }
    var direction = 'stop'
    keyEvent()
    //怪兽方向
    function animate() {
      
      if(bulletShoot){
        // plane.shoot(bullets)
      }
      if(planeDirection === 'right'){
        plane.move(CONFIG.planeSpeed)
      }else if(planeDirection === 'left'){
        plane.move(0 - CONFIG.planeSpeed)
      }
      //计算怪兽爆炸后boom的帧数
      for (let i = 0; i < enemys.length; i++) {
        if (enemys[i].boom === 4) {
          score++
          enemys = enemys.delete(i--)
        }
      }
      if (enemys.length > 0) {
        if (bullets.length > 0) {
          //子弹碰撞怪兽
            for (let j = 0; j < enemys.length; j++) {
              for (let i = 0; i < bullets.length; i++) {
                if (!(bullets[i].x + 1 < enemys[j].x) &&
                  !(enemys[j].x + CONFIG.enemySize < bullets[i].x) &&
                  !(bullets[i].y + CONFIG.bulletSize < enemys[j].y) &&
                  !(enemys[j].y + CONFIG.enemySize < bullets[i].y)) {
                  // 物体碰撞了
                  bullets = bullets.delete(i--) //数组删除一个元素的时候，数组长度减1，后面点元素就会往前移动一位，索引也减1，但是i还是进行了i++的操作。所以这里i要先减1，i--即为先使用i的值，在对i进行减一
                  
                  enemys[j].boom = 1 //怪兽爆炸变为爆炸图片
                  // enemys = enemys.delete(j)
                }
            }
          }
        } 
      }else {
        // console.log(enemys.length)
        canvas.style.display = 'none'
        // debugger
        if (CONFIG.lineNums >= CONFIG.totalLevel) {
          // debugger
          score3.innerHTML = score
          self.end('all-success')
          return false
        } else {
          // debugger
          score2.innerHTML = score
          self.end('success')
          return false
        }
      }

      //更新怪兽位置
      if (enemys.length > 0) {
        if (enemys[enemys.length-1].y > enemyPlace.maxY) {
          canvas.style.display = 'none'
          score1.innerHTML = score
          self.end('failed')
          return false //不return false 的话， animate还会继续执行
        }
        enemyXArr = []
        enemys.forEach((enemy)=>{
          enemyXArr.push(enemy.x)
        })
        enemyXArr.sort(num)
        if (enemyXArr[enemyXArr.length - 1] >= enemyPlace.maxX) {
          direction = 'left'
          enemys.forEach((enemy) => {
            enemy.y += CONFIG.enemySpeedY
          })
        } else if (enemyXArr[0] <= enemyPlace.minX) {
          direction = 'right'
          enemys.forEach((enemy) => {
            enemy.y += CONFIG.enemySpeedY
          })
        }
      }

      enemys.forEach((enemy) => {
        enemy.move(direction)
      })
      // 更新子弹位置
      bullets.forEach((bullet) => {
        bullet.move()
      })
      //把超出边界的子弹过滤掉
      bullets = bullets.filter(bullet => bullet.y > padding)

      // 清除画布
      context.clearRect(0, 0, canvas.width, canvas.height);
      // 绘画怪兽
      
      enemys.forEach((enemy, index) => {
        if (enemy.boom) {
          enemy.boom++
          enemy.draw(CONFIG.enemyBoomIcon, CONFIG.enemySize, CONFIG.enemySize)
        } else {
          
          enemy.draw(CONFIG.enemyIcon, CONFIG.enemySize, CONFIG.enemySize)
        }

      })
      // 绘画子弹
      bullets.forEach((bullet) => {
        bullet.draw()
      })
      plane.draw(CONFIG.planeIcon, CONFIG.planeSize.width, CONFIG.planeSize.height)
      context.font = '20px serif';
      context.fillStyle = 'white';
      
      context.fillText('当前分数:'+ score, 20, 60);
      context.fillText('当前关卡:'+ lineNums, 20, 30);
      // 使用requestAnimationFrame实现动画循环
      requestAnimationFrame(animate);
    }
    animate()
  },
  end: function (status) {
    this.setStatus(status);
  },
};


// 初始化
GAME.init()

