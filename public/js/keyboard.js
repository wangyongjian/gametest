/**
 * 键盘操作相关对象
 */
var KeyBoard = function(){
	var self = this;
	//键盘按下
	document.addEventListener("keydown",function(event){
		self.keydown(event)
	});
	//键盘放开
	document.addEventListener("keyup",function(event){
		self.keyup(event)
	});
}

KeyBoard.prototype = {
	pressedLeft: false, //是否点击左边
	pressedRight: false, //是否点击右边
	pressedUp: false, //是否点击上边
	pressedSpace: false, //是否点击空格
	keydown: function(event) {
		//获取键位
		var key = event.keyCode;
		switch(key) {
			//点击空格
			case 32:
			this.pressedSpace = true;
			break;
			//点击向左
			case 37:
			this.pressedLeft = true;
			this.pressedRight = false;
			break;
			//点击向上
			case 38:
			this.pressedUp = true;
			break;
			//点击向右
			case 39:
			this.pressedLeft = false;
			this.pressedRight = true;
			break;
			default: alert("左右箭头移动飞机，往上或者空格发射子弹");
		}
	},
	keyup: function(event) {
		//获取键位
		var key = event.keyCode;
		switch(key) {
			case 32:
			this.pressedSpace = false;
			break;
			case 37:
			this.pressedLeft = false;
			break;
			case 38:
			this.pressedUp = false;
			break;
			case 39:
			this.pressedRight = false;
			break;
			default: alert("左右箭头移动飞机，往上或者空格发射子弹");
		}
	}
};