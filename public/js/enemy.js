function Enemy(x, y) {
    Element.call(this, x, y)
}
// inheritPrototype(Enemy, Element)
Enemy.prototype.move = function(direction) {
    if(direction === 'right'){
        this.x += CONFIG.enemySpeedX
    }else if(direction === 'left'){
        this.x -= CONFIG.enemySpeedX
    }
}
Enemy.prototype.draw = function (image, width, height) {
    context.drawImage(image, this.x, this.y, width, height)
}
Enemy.prototype.enemyBoom = function () {
    context.drawImage(CONFIG.enemyBoom, this.x, this.y, width, height)
}