function loadImages(sources, callback) {
    var loadedImages = []
    var loadedImagesNum = 0;
    var numImages = sources.length;
    for( let i = 0; i < numImages; i++) {
        loadedImages[i] = new Image();
        loadedImages[i].src = sources[i]
        loadedImages[i].onload = function() {
            loadedImagesNum ++;
            if(loadedImagesNum == numImages){
                callback(loadedImages)
            }
        }
    }
}

//继承原型的函数
function inheritPrototype(subType, superType){
    let proto = superType.prototype;
    proto.constructor = subType;
    subType.prototype = proto
}

 // 兼容定义 requestAnimFrame
 window.requestAnimFrame =
 window.requestAnimationFrame ||
 window.webkitRequestAnimationFrame ||
 window.mozRequestAnimationFrame ||
 window.oRequestAnimationFrame ||
 window.msRequestAnimationFrame ||
 function(callback) {
     window.setTimeout(callback, 1000 / 30);
 };
 //数组根据index索引删除元素
Array.prototype.delete=function(delIndex){
    var temArray=[];
    for(var i=0;i<this.length;i++){
        if(i!=delIndex){
            temArray.push(this[i]);
        }
    }
    // console.log(temArray)
    return temArray;
}
    
